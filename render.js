const ejs = require('ejs');
const path = require("path");
var QRCode = require('qrcode')

let options = [
    { errorCorrectionLevel: 'L' },
    { type: 'terminal' },
    { mode: 'alphanumeric' }
]

// console.log('------------------>Done QRcode<<<<<<<-----');
QRCode.toDataURL(`UGG-BB-PUR-06-5e565375dcb5002f8331f519`, options, function(err, url) {
    let filepath = path.join(__dirname + "/render.ejs")
    console.log('------------------>filepath<<<<<<<-----', filepath);
    ejs.renderFile(filepath, { data: url }, (err, html) => {
        console.log('------------------>filepath<<<<<<<-----', typeof url, );
        if (err) {
            console.log('u got err-->', err)
                // reject(err);
        } else {
            // resolve(html);
            console.log('---done ----', html)
        }
    })
})

/*******************************************************/
let searchAsset = await dbAsset.findOne({ warehouseId: req.body.warehouseId, spaceId: req.body.spaceId });
// console.log('searchAsset>>>>>', searchAsset)

if (searchAsset) {
    // let cbLink = req.app.get("cbLink");
    let qrCode = req.body.spaceId;

    let qrCodeData = await qrCodeModule(qrCode)
    console.log('qrCode  :::::::>', qrCodeData)

    let html = await renderHTML(path.join(__dirname + "../../../common/pdf_templates/singleBarcode.ejs"), { data: qrCodeData });
    let pdf = await generatePDF(html, "barcode");
    console.log('qrCode data :::::::>', html)

    await dbAsset.update({ warehouseId: req.body.warehouseId, spaceId: req.body.spaceId }, { $set: { qrCodeGenerated: true, qrCode } })

    // res.send(html)
    res.json({
        success: true,
        msg: 'Barcode generated successfully for assets.',
        buffer: pdf,
    })
} else {
    res.json({
        success: false,
        msg: 'No such asset found for given Id.'
    })
}